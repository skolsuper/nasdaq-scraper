# README #

### Start the scraper + server ###

`docker-compose up scraper server`

Scraper polling interval (milliseconds) can be set by the env var `POLLING_INTERVAL`.

Then, results will be available at `http://localhost:3000/`.

### Run the tests ###

`docker-compose up tests`