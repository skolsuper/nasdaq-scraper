const chai = require('chai'), should = chai.should();
const chaiHttp = require('chai-http');

const server = require('../src/server');

chai.use(chaiHttp);

describe('Nasdaq API server', function () {
  it('should respond to a GET request', function (done) {
    chai.request(server)
      .get('/')
      .end(function (err, res) {
        res.should.have.status(200);
        done();
      });
  });
});
