const chai = require('chai'), expect = chai.expect;
const sinon = require('sinon');

const scraper = require('../src/scraper');

describe('Nasdaq scraper', function () {

  describe('Index value retrieve function', function () {

    it('should extract the index value from the homepage table html', function () {
      const exampleHtml = `<table id="indexTable" class="floatL marginB5px">
        <thead>
        <tr>
        <th>Index</th>
        <th>Value</th>
        <th>Change Net / %</th>
      </tr>
      </thead>
      <script type="text/javascript">
        //<![CDATA[
      nasdaqHomeIndexChart.storeIndexInfo("NASDAQ","5237.57","8.91","0.17","704,338,450","5243.17","5221.84");
      nasdaqHomeIndexChart.storeIndexInfo("DJIA","18561.71","-12.23","0.07","","18600.82","18551.27");
      nasdaqHomeIndexChart.storeIndexInfo("S&P 500","2183.45","1.23","0.06","","2186.36","2180.46");
      nasdaqHomeIndexChart.storeIndexInfo("NASDAQ-100","4808.29","2.84","0.06","","4814.54","4796.84");
      nasdaqHomeIndexChart.storeIndexInfo("NASDAQ-100 PMI","4802.91","-2.54","0.05","","4802.96","4801.93");
      nasdaqHomeIndexChart.storeIndexInfo("NASDAQ-100 AHI","4804.79","-0.66","0.01","","4806.52","4802.07");
      nasdaqHomeIndexChart.storeIndexInfo("Russell 1000","1209.88","1.77","0.15","","1210.75","1207.30");
      nasdaqHomeIndexChart.storeIndexInfo("Russell 2000","1235.90","8.22","0.67","","1236.19","1227.42");
      nasdaqHomeIndexChart.storeIndexInfo("FTSE All-World ex-US*","277.35","-0.09","0.03","","277.34","277.34");
      nasdaqHomeIndexChart.storeIndexInfo("FTSE RAFI 1000*","9538.63","9.70","0.10","","9538.63","9538.63");
      //]]>
      nasdaqHomeIndexChart.displayIndexes();
      </script><tbody><tr id="indexTableRow0" onclick="nasdaqHomeIndexChart.toggleChartData('NASDAQ')" class="selected">
        <td><b>NASDAQ</b></td>
        <td>5237.57</td>
        <td style="color:#387C2C"><div class="indexChange">8.91&nbsp;<span>▲</span>&nbsp;0.17%</div></td>
      </tr>
      <tr id="indexTableRow1" onclick="nasdaqHomeIndexChart.toggleChartData('NASDAQ-100')">
        <td><b>NASDAQ-100 (NDX)</b></td>
        <td>4808.29</td>
        <td style="color:#387C2C"><div class="indexChange">2.84&nbsp;<span>▲</span>&nbsp;0.06%</div></td>
      </tr>
      <tr id="indexTableRow2" onclick="nasdaqHomeIndexChart.toggleChartData('NASDAQ-100 PMI')">
        <td><b>Pre-Market (NDX)</b></td>
        <td>4802.91</td>
        <td style="color:#EE3524"><div class="indexChange">-2.54&nbsp;<span>▼</span>&nbsp;0.05%</div></td>
      </tr>
      <tr id="indexTableRow3" onclick="nasdaqHomeIndexChart.toggleChartData('NASDAQ-100 AHI')">
        <td><b>After Hours (NDX)</b></td>
      <td>4804.79</td>
      <td style="color:#EE3524"><div class="indexChange">-0.66&nbsp;<span>▼</span>&nbsp;0.01%</div></td>
      </tr>
      <tr id="indexTableRow4" onclick="nasdaqHomeIndexChart.toggleChartData('DJIA')">
        <td><b>DJIA</b></td>
        <td>18561.71</td>
        <td style="color:#EE3524"><div class="indexChange">-12.23&nbsp;<span>▼</span>&nbsp;0.07%</div></td>
      </tr>
      <tr id="indexTableRow5" onclick="nasdaqHomeIndexChart.toggleChartData('S&amp;P 500')">
        <td><b>S&amp;P 500</b></td>
      <td>2183.45</td>
      <td style="color:#387C2C"><div class="indexChange">1.23&nbsp;<span>▲</span>&nbsp;0.06%</div></td>
      </tr>
      <tr id="indexTableRow6" onclick="nasdaqHomeIndexChart.toggleChartData('Russell 2000')">
        <td><b>Russell 2000</b></td>
      <td>1235.90</td>
      <td style="color:#387C2C"><div class="indexChange">8.22&nbsp;<span>▲</span>&nbsp;0.67%</div></td>
      </tr>

      </tbody></table>`;
      const expectedValue = '5237.57';
      const actual = scraper.retrieveIndexValue(exampleHtml);
      actual.should.equal(expectedValue);
    });
  });

  describe('Load value to db function', function () {
    it('should pass the value to the store with the timestamp as the id', function () {
      const input = {value: '1234.56', timestamp: 1483657456};
      const expected = {value: 1234.56, _id: 1483657456};
      const spy = sinon.spy();
      const mockdb = {
        nasdaqValues: {
          insert: spy,
        },
      };
      scraper.$$loadValueToDb(input, mockdb);
      expect(spy.called);
      expect(spy.calledWith(expected));
    });
  });

});
