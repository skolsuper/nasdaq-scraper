FROM node:6

RUN mkdir -p /usr/dev/app
WORKDIR /usr/dev/app

COPY package.json /usr/dev/app/package.json
RUN npm install

COPY .eslintrc /usr/dev/app/.eslintrc

VOLUME /usr/dev/app/src
VOLUME /usr/dev/app/test
