/**
 * Scraper that retrieves the index value from the nasdaq.com homepage and store it in the db.
 */

const Rx = require('rx');
const request = require('request');
const cheerio = require('cheerio');

const db = require('./db');

const POLLING_INTERVAL = process.env.POLLING_INTERVAL || 60000;  // ms
const NASDAQ_URL = process.env.NASDAQ_URL || 'http://www.nasdaq.com/';

/**
 * Takes the index value+timestamp extracted from the homepage and stores it in the database.
 * Convert the value to a Number before storing it in case we want to do min/max/avg-type
 * queries in the future.
 * @param result: Object
 * @param result.value: String
 * @param result.timestamp: Number
 * @param store: Injectable data store object.
 * @param store.nasdaqValues: Collection to insert data.
 */
function $$loadValueToDb(result, store) {
  console.log('loading result to db', result);
  store.nasdaqValues.insert({
    _id: result.timestamp,  // using timestamp as _id saves creating an extra index
    value: parseFloat(result.value),
  }, (err, doc) => {
    console.log('Saved!', doc);
  });
}

/**
 * Wrapper that calls private $$loadValueToDb using the mongodb database connection as a datastore.
 * @param result
 */
function loadValueToDb(result) {
  return $$loadValueToDb(result, db);
}

/**
 * Takes the raw html of the Nasdaq homepage and returns the value of the main index.
 */
function retrieveIndexValue(html) {
  console.log('retrieving index value');
  const $ = cheerio.load(html);
  const data = $('#indexTable').find('script').text();
  const match = /"NASDAQ","([.\d]+)"/.exec(data);
  return match[1];
}

/**
 * Makes a `requests` request that gets the nasdaq homepage as an RxJs Observable.
 */
function getNasdaqHomepage(url) {
  console.log('Getting homepage');
  const rxRequest = Rx.Observable.fromNodeCallback(request);
  return rxRequest(url);
}

const ticks = Rx.Observable.interval(POLLING_INTERVAL);

const indexValue = ticks.map(() => NASDAQ_URL)
  .flatMap(getNasdaqHomepage)
  .pluck(1) // Get the 2nd argument to the request callback, 'body'
  .map(retrieveIndexValue)
  .timestamp();

console.log('Starting scraper!');
indexValue.subscribe(loadValueToDb, console.error, () => console.log('Exit.'));

module.exports = {
  retrieveIndexValue,
  getNasdaqHomepage,
  loadValueToDb,
  $$loadValueToDb,
};
