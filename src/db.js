/**
 * Store the data from the scraper to be queried by express.
 */

const mongojs = require('mongojs');

const db = mongojs('db/test', ['nasdaqValues']);

module.exports = db;
