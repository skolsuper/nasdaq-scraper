const express = require('express');
const _ = require('lodash');

const db = require('./db');

const collection = db.nasdaqValues;

const MAX_LIMIT = 1000;
const DEFAULT_LIMIT = 50;

const app = express();

/**
 * Simply changes the _id key to something more self-documenting.
 */
function convertToRepr(item) {
  return {
    value: item.value,
    timestamp: item._id,  // eslint-disable-line no-underscore-dangle
  };
}

/**
 * Returns the latest 50 results by default, configurable by a `limit` query param up to 1000.
 * Returned data is in the schema: {
 *  count: Number
 *  results: Array<{value: Number, timestamp: Number}>
 * }
 */
app.get('/', (req, res) => {
  let limit = req.params.limit || DEFAULT_LIMIT;
  limit = _.min([limit, MAX_LIMIT]);
  collection.find().limit(limit, (err, docs) => {
    const latestValues = _(docs).map(convertToRepr).value();
    res.json({
      count: latestValues.length,
      results: latestValues,
    });
  });
});

app.listen(3000, () => {
  console.log('Listening on port 3000');
});

module.exports = app;
